from ..logger import LOGGER


class SecondaryTaskProvider:
    def __init__(self, storage):
        self._storage = storage

    def get_task(self):
        LOGGER.info("Getting task...")
        return self._storage.get_task()  # TODO:Rename _storage.get_task() to correct storage object function
